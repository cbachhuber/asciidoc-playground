A matrix

[stem]
++++
M = \begin{bmatrix}
f_x & & p_x \\
& f_y & py \\
& & 1
\end{bmatrix}
++++

a sum

[stem]
++++
M = \begin{bmatrix}
f_x & & p_x \\
& f_y & py \\
& & 1
\end{bmatrix}
++++

another matrix 

[stem]
++++
\boldsymbol{x}_{\textrm{image}} =

\begin{bmatrix}
x_{\textrm{image}} \\ y_{\textrm{image}}
\end{bmatrix} =

\begin{bmatrix}
p_x \\ p_y
\end{bmatrix}

- x_{\textrm{world}}^{-1}

\begin{bmatrix}
f_x y_{\textrm{world}} \\
f_y z_{\textrm{world}}
\end{bmatrix},
++++

and another matrix

[stem]
++++
\hat{r} = \begin{bmatrix}
\cos{\phi} \cos{\theta} \\
\sin{\phi} \cos{\theta} \\
\sin{\theta}
\end{bmatrix}
++++
